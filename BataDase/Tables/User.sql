﻿CREATE TABLE [dbo].[User]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	LastName varchar(20) not null,
	Firstname varchar(20),
	Login varchar(20) unique,
	Password varbinary(64),
	Salt varchar(100),
	IsAdmin bit default (0)

)
