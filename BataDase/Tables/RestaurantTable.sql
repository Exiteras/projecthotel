﻿CREATE TABLE [dbo].[RestaurantTable]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	SittingPlaces int,
	IsReserved bit default (0)
)
