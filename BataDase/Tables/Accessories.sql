﻿CREATE TABLE [dbo].[Accessories]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	Name varchar(30),
	Quantity int, 
    CONSTRAINT [CK_Accessories_Quantity] CHECK (Quantity>0)
)
