﻿using HotelResa.DAL.Services;
using HotelResa.DTO.Customer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mappers;
using System.Data.SqlClient;

namespace HotelResa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerService _customerService;

        public CustomerController(CustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpPost]
        public IActionResult Post([FromBody]CustomerRegister body)
        {
            try
            {
                Customer c = new();
                c = _customerService.Add(body.Map<DAL.Entities.Customer>()).Map<Customer>();
                return Ok(c);
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627) return Problem("Customer already exist");
                return Problem(ex.Message);

            }
        }
    }
}
