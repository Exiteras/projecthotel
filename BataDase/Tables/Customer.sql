﻿CREATE TABLE [dbo].[Customer]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	LastName varchar(20) not null,
	FirstName varchar(20),
	BirthDate datetime,
	PhoneNumber varchar(50),
	Email varchar(30) unique,
	CreditCard varchar(30),
	NoshowCount int,
	ReservationTableCount int default (0),
	BlackListRestaurant bit default (0)

)
