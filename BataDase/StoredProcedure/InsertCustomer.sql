﻿CREATE PROCEDURE [dbo].[InsertCustomer]
	@lastName varchar(30),
	@firstname varchar(30),
	@birthdate date,
	@phonenumber varchar(40),
	@email varchar(30),
	@creditcard varchar(40)
AS
	insert into Customer (BirthDate,FirstName,LastName,PhoneNumber,CreditCard,Email) values (@birthdate,@firstname,@lastName,@phonenumber,@creditcard,@email)
	select * from Customer where Email = @email
RETURN 0
