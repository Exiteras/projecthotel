﻿CREATE TABLE [dbo].[HotelRoom]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	QuantitySingleBed int,
	QuantityDoubleBed int,
	RoomName varchar(20),
	IsReserved bit
)
