﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelResa.DAL.Entities
{
    public class HotelRoom
    {
        public int Id { get; set; }
        public int QuantitySingleBed { get; set; }
        public int QuantityDoubleBed { get; set; }
        public string RoomName { get; set; }
        public bool IsReserved { get; set; }

    }
}
