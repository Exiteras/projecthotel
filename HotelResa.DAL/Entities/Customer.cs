﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelResa.DAL.Entities
{
     public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string CreditCard { get; set; }
        public int NoShowCount { get; set; }
        public int ReservationTableCount { get; set; }
        public bool BlackListRestaurant { get; set; }
    }
}
