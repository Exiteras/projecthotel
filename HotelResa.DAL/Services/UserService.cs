﻿using ADOLibrary;
using HotelResa.DAL.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Mappers;

namespace HotelResa.DAL.Services
{
    public class UserService
    {
        
        private Connection _connection { get; set; }

        public UserService(IConfiguration config)
        {
            
            ConnectionDb c = new(config.GetConnectionString("Default"));
            _connection = c.Connection;
        }
        public User Convert(IDataReader reader)
        {
            return new User
            {
                Id = (int)reader["Id"],
                IsAdmin = (bool)reader["IsAdmin"],
                FirstName = reader["FirstName"].ToString(),
                LastName = reader["Lastname"].ToString(),
                Login = reader["Login"].ToString()
            };
        }
        public void Add(User user)
        {
            using (TransactionScope scope = new())
            {
                try
                {
                    Command c = new Command("InsertUser",true);
                    c.AddParameter("@lastName", user.LastName);
                    c.AddParameter("@firstName", user.FirstName);
                    c.AddParameter("@login", user.Login);
                    c.AddParameter("@password", user.Password);
                    _connection.ExecuteNonQuery(c);
                    scope.Complete();

                }
                catch (Exception)
                {

                    throw;
                }

            }
        }
        public User Login(string pwd, string login)
        {
            Command c = new("LoginUser", true);
            c.AddParameter("@login", login);
            c.AddParameter("@password", pwd);
            return _connection.ExecuteReader<User>(c,Convert).FirstOrDefault();
        }
        public User GetById(int id)
        {
            Command c = new Command("Select * from V_User WHERE Id = @Id");
            c.AddParameter("@Id", id);
            return _connection.ExecuteReader<User>(c, Convert).FirstOrDefault();
        }
    }
}
