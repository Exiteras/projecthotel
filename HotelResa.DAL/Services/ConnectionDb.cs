﻿using ADOLibrary;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelResa.DAL.Services
{
    public class ConnectionDb
    {
        public Connection Connection { get; set; }
        

        public ConnectionDb(string connectionstring)
        {
            
            Connection = new Connection(SqlClientFactory.Instance, connectionstring);

        }
       
    }
}
