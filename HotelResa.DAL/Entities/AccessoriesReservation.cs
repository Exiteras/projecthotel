﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelResa.DAL.Entities
{
    public class AccessoriesReservation
    {
        public int Id { get; set; }
        public int ReservationId { get; set; }
        public int AccessorieId { get; set; }
    }
}
