﻿using D = HotelResa.DAL.Entities;
using HotelResa.DAL.Services;
using HotelResa.DTO;
using Mappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace HotelResa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly DAL.Services.UserService _userServices;

        public UserController(UserService userServices)
        {
            this._userServices = userServices;
        }
        [HttpPost]
        public IActionResult Post([FromBody]UserRegister body)
        {
            try
            {
                _userServices.Add(body.Map<D.User>());
                return NoContent();
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627) return Problem("Login already exist");
                return Problem(ex.Message);
            
            }
        }
        [HttpPost]
        [Route("login")]
        public IActionResult Post([FromBody] UserLogin body)
        {
            if (body is null) return BadRequest("UserNull");
            
            try
            {
                DTO.User u = new();
                u = _userServices.Login(body.Password, body.Login).Map<DTO.User>();
                if (u is null) return new ForbidResult("Interdit");
                
                return Ok(u);

            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpGet]
        public IActionResult Get(int Id)
        {
            try
            {
                DTO.User u = new DTO.User();
                u = _userServices.GetById(Id).Map<DTO.User>();
                return Ok(u);
            }
            catch (Exception ex)
            {
                
                return Problem(ex.Message);
                
            }
        }
    }
}
