﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelResa.DAL.Entities
{
    public class TableReservation
    {
        public int Id { get; set; }
        public int TableId { get; set; }
        public DateTime ReservationDate { get; set; }
        public int QuantityCustomer { get; set; }
        public bool NoShow { get; set; }
    }
}
