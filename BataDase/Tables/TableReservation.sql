﻿CREATE TABLE [dbo].[TableReservation]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	TableId int not null,
	CustomerId int,
	ReservationDate datetime,
	QuantityCustomer int,
	NoShow bit default (0), 
    CONSTRAINT [FK_TableReservation_ToTable] FOREIGN KEY ([TableId]) REFERENCES [RestaurantTable]([Id]), 
    CONSTRAINT [FK_TableReservation_ToCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [Customer]([Id])
)
