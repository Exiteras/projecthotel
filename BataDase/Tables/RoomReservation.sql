﻿CREATE TABLE [dbo].[RoomReservation]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	RoomId int not null,
	CustomerId int not null,
	Arrivaldate datetime not null,
	DepartureDate datetime not null,
	QuantityCustomer int,
	TotalPrice money,
	AdvancePayment money,
	RestToPay money,
	CustomerAsk varchar(100), 
    CONSTRAINT [FK_RoomReservation_ToCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [Customer]([Id]), 
    CONSTRAINT [FK_RoomReservation_ToRoom] FOREIGN KEY ([RoomId]) REFERENCES [HotelRoom]([Id]), 
    

)
