﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelResa.DAL.Entities
{
    public class RestaurantTable
    {
        public int Id { get; set; }
        public int SittingPlaces { get; set; }
        public bool IsReserved { get; set; }

    }
}
