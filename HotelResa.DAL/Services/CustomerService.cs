﻿using ADOLibrary;
using HotelResa.DAL.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace HotelResa.DAL.Services
{
    public class CustomerService
    {

        private Connection _co { get; set; }
        public CustomerService(IConfiguration config)
        {
            
            ConnectionDb c = new(config.GetConnectionString("Default"));
            _co = c.Connection;

        }
        private Customer Convert(IDataRecord reader)
        {
            return new Customer
            {
                CreditCard = reader["CreditCard"] is DBNull ? null : reader["CreditCard"].ToString(),
                BirthDate = reader["BirthDate"] is DBNull ? null : (DateTime)reader["BirthDate"],
                FirstName = reader["FirstName"].ToString(),
                Email = reader["Email"].ToString(),
                LastName = reader["LastName"].ToString(),
                PhoneNumber = reader["PhoneNumber"].ToString(),
                Id = (int)reader["Id"]
            };
        }

        public Customer Add(Customer c)
        {
            using (TransactionScope scope = new())
            {
                Customer cc = new();
                try
                {
                    Command cmd = new("InsertCustomer", true);
                    cmd.AddParameter("@birthdate", c.BirthDate is null ? DBNull.Value : c.BirthDate );
                    cmd.AddParameter("@firstName", c.FirstName);
                    cmd.AddParameter("@lastName", c.LastName);
                    cmd.AddParameter("@phonenumber", c.PhoneNumber);
                    cmd.AddParameter("@creditcard", c.CreditCard is null? DBNull.Value : c.CreditCard);
                    cmd.AddParameter("@email", c.Email);
                    cc = _co.ExecuteReader<Customer>(cmd, Convert).FirstOrDefault();
                    scope.Complete();
                    return cc;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }
}
