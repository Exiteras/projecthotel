﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelResa.DAL.Entities
{
    public class RoomReservation
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int CustomerId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public int QuantityCustomer { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal AdvancePayment { get; set; }
        public decimal ResToPay { get; set; }
        public string CustomerAsk { get; set; }
    }
}
