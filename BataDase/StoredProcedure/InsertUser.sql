﻿CREATE PROCEDURE [dbo].[InsertUser]
	@lastName varchar(20),
	@firstName varchar(20),
	@login varchar(20),
	@password varchar(30)
	
	
AS
	BEGIN
		
		declare @salt varchar(100)
		set @salt = concat(newid(),newid(),newid())
		declare @secret varchar(100) = dbo.GetSecretKey()
		declare @hashpassword varbinary(64)
		set @hashpassword = HASHBYTES('SHA2_512',CONCAT(@salt,@password,@secret,@salt))
		insert into [User] (LastName,Firstname,Login,Password,Salt) values (@lastName,@firstName,@login,@hashpassword,@salt)
	END
RETURN 0
