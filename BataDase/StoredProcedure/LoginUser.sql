﻿CREATE PROCEDURE [dbo].[LoginUser]
	@login varchar(50),
	@password varchar(50)
AS
	declare @salt varchar(100);
	set @salt = (select Salt from [User] where Login = @login)
	declare @secret varchar(100) = dbo.GetSecretKey()
	declare @hashpassword varbinary(64)
	set @hashpassword = HASHBYTES('SHA2_512',concat(@salt,@password,@secret,@salt))
	declare @id int
	set @id = (select Id from [User] where Login = @login and Password = @hashpassword)
	select * from V_User where Id = @id
RETURN 0
