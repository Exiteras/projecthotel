﻿CREATE TABLE [dbo].[AccessoriesReservation]
(
	[Id] INT NOT NULL PRIMARY KEY identity,
	ReservationId int,
	AccessorieId int, 
    CONSTRAINT [FK_AccessoriesReservation_Room] FOREIGN KEY ([ReservationId]) REFERENCES [RoomReservation]([Id]), 
    CONSTRAINT [FK_AccessoriesReservation_Accessorie] FOREIGN KEY ([AccessorieId]) REFERENCES [Accessories]([Id])
)
